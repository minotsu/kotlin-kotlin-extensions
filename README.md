[![official](https://img.shields.io/badge/official-URL-orange.svg)](https://bitbucket.org/minotsu/kotlin-kotlin-extensions)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://spdx.org/licenses/MIT.html)

# 美濃通Kotlin拡張ライブラリ
これは美濃通が作成したKotlin拡張ライブラリです。
