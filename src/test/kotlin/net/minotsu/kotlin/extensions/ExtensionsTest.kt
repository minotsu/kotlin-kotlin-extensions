/*
 * © 2020 美濃通
 *
 * このソフトウェアのいかなる部分も、制作者の書面による許可なしに、いかなる形式
 * や電子的または機械的手段によっても複製できません。
 */

@file:Suppress("NonAsciiCharacters", "SpellCheckingInspection", "TestFunctionName", "unused")

package net.minotsu.kotlin.extensions

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import kotlin.contracts.ExperimentalContracts

class ExtensionsTest {

    @Test
    @ExperimentalContracts
    fun 全ての引数がnullでない場合はブロックの評価値を返す() {
        val s1: String? = "a"
        val s2: String? = "b"
        val s3: String? = "c"
        val s4: String? = "d"

        assertThat(runWith(s1, s2) { p1, p2 -> p1 + p2 }).isEqualTo("ab")
        assertThat(runWith(s1, s2, s3) { p1, p2, p3 -> p1 + p2 + p3 }).isEqualTo("abc")
        assertThat(runWith(s1, s2, s3, s4) { p1, p2, p3, p4 -> p1 + p2 + p3 + p4 }).isEqualTo("abcd")
    }

    @Test
    @ExperimentalContracts
    fun いづれかの引数がnullであればブロックを実行せずnullを返す() {
        val s1: String? = "a"
        val s2: String? = "b"
        val s3: String? = "c"
        val sn: String? = null

        assertThat(runWith(s1, sn) { _, _ -> throw Exception() } as String?).isNull()
        assertThat(runWith(s1, s2, sn) { _, _, _ -> throw Exception() } as String?).isNull()
        assertThat(runWith(s1, s2, s3, sn) { _, _, _, _ -> throw Exception() } as String?).isNull()
    }
}
