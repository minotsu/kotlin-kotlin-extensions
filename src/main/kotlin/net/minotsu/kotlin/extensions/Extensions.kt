/*
 * © 2020 美濃通
 *
 * このソフトウェアのいかなる部分も、制作者の書面による許可なしに、いかなる形式
 * や電子的または機械的手段によっても複製できません。
 */

@file:Suppress("RedundantVisibilityModifier", "unused")

package net.minotsu.kotlin.extensions

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

/** [p1], [p2]がnullでなければ[block]の戻り値を返し、そうでなければnullを返す. */
@ExperimentalContracts
public fun <T1, T2, R> runWith(p1: T1?, p2: T2?, block: (T1, T2) -> R?): R? {
    contract {
        returnsNotNull() implies (p1 != null && p2 != null)
    }

    return if (p1 != null && p2 != null) block(p1, p2) else null
}

/** [p1], [p2], [p3]がnullでなければ[block]の戻り値を返し、そうでなければnullを返す. */
@ExperimentalContracts
public fun <T1, T2, T3, R> runWith(p1: T1?, p2: T2?, p3: T3?, block: (T1, T2, T3) -> R?): R? {
    contract {
        returnsNotNull() implies (p1 != null && p2 != null && p3 != null)
    }

    return if (p1 != null && p2 != null && p3 != null) block(p1, p2, p3) else null
}

/** [p1], [p2], [p3], [p4]がnullでなければ[block]の戻り値を返し、そうでなければnullを返す. */
@ExperimentalContracts
public fun <T1, T2, T3, T4, R> runWith(
    p1: T1?,
    p2: T2?,
    p3: T3?,
    p4: T4?,
    block: (T1, T2, T3, T4) -> R?
): R? {
    contract {
        returnsNotNull() implies (p1 != null && p2 != null && p3 != null && p4 != null)
    }

    return if (p1 != null && p2 != null && p3 != null && p4 != null) block(p1, p2, p3, p4) else null
}
