/*
 * © 2020 美濃通
 *
 * このソフトウェアのいかなる部分も、制作者の書面による許可なしに、いかなる形式
 * や電子的または機械的手段によっても複製できません。
 */

val vendorName: String by project
val jvmTarget: String by project
val publicationName = "maven"

val ver = mapOf(
    "junit" to "5.6.2"
)

plugins {
    `java-library`
    kotlin("jvm") version "1.3.72"
    `maven-publish`
    signing
    id("com.jfrog.bintray") version "1.8.4"
    id("org.jlleitschuh.gradle.ktlint") version "9.2.1"
    id("org.jetbrains.dokka") version "0.10.1"
    id("com.github.ben-manes.versions") version "0.28.0"
}

repositories {
    mavenCentral()
    jcenter()
}

ktlint {
    verbose.set(true)
    outputToConsole.set(true)
    coloredOutput.set(true)

    reporters {
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.CHECKSTYLE)
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.JSON)
    }

    filter {
        exclude("**/style-violations.kt")
    }
}

afterEvaluate {
    publishing {
        publications {
            create<MavenPublication>(publicationName) {
                from(components["java"])
                artifact(tasks["kotlinSourcesJar"])
                artifact(tasks["kdocsJar"])
                groupId = "$group"
                artifactId = "${property("artifactId")}"
                version = "${project.version}"

                pom {
                    description.set(project.description)
                    url.set("${property("websiteUrl")}")
                }
            }
        }
    }

    signing {
        sign(publishing.publications[publicationName])
    }

    bintray {
        setPublications(publicationName)
        user = property("bintray.user") as String
        key = property("bintray.key") as String
        publish = true

        pkg.apply {
            repo = "maven"
            name = "$group.${property("artifactId")}"
            userOrg = vendorName.toLowerCase()
            setLicenses("MIT")
            vcsUrl = "${property("websiteUrl")}.git"
            websiteUrl = "${property("websiteUrl")}"
            desc = description

            version.apply {
                name = "${project.version}"
            }
        }
    }

    tasks {
        named("sign${publicationName.capitalize()}Publication") {
            group = "publishing"
        }
    }
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = jvmTarget
    }

    compileTestKotlin {
        kotlinOptions.jvmTarget = jvmTarget
    }

    test {
        useJUnitPlatform()
    }

    jar {
        manifest {
            attributes(
                mapOf(
                    "Manifest-Version" to "1.0",
                    "Implementation-Title" to project.name,
                    "Implementation-Version" to project.version,
                    "Implementation-Vendor" to vendorName
                )
            )
        }
    }

    dokka {
        outputFormat = "html"
    }

    register<Jar>("kdocsJar") {
        dependsOn("dokka")
        archiveClassifier.set("kdoc")
        from(dokka.get().outputDirectory)
    }
}

dependencies {
    implementation(kotlin("stdlib-jdk7"))
    implementation("org.slf4j:slf4j-api:2.0.0-alpha1")
    testImplementation("org.junit.jupiter:junit-jupiter-api:${ver["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${ver["junit"]}")
    testImplementation("org.assertj:assertj-core:3.16.1")
}
